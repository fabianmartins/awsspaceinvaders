
{
  "ClosingTime": "2018-10-26T00:18:36.240Z",
  "GameType": "SINGLE_TRIAL",
  "OpeningTime": "2018-10-26T00:18:34.013Z",
  "SessionId": "2018-10-26T00:18:34",
  "Synchronized": false
}

    {
      "Level": 1,
      "Lives": 0,
      "Nickname": "fabian.martins",
      "Score": 185,
      "Shots": 46,
      "Timestamp": "2018-10-26T15:43:19.324Z"
    }

FEATURES

(A) GAME CONTROL (for both)
A1. Response to disconnection
A2. Clean up of resources at the end (close kinesis etc);
A3. Adjust the START GAME PERMISSIONS
A5. custom:hasAlreadyPlayed: 
    IF the game is TIME-CONSTRAINED, change it when the time has finished (for all users) - REQUIRES WEBSOCKET
    IF the game is ONE-TRIAL, change it when the user LIVES hits ZERO (in the back-end)
    IF the game is MULTIPLE TRIALS, allow players to participate how many times they want

(B) GAME USER EXPERIENCE
B1. Response to start while disconnected.
B2. Response to restart on the same game.
B3. Implement cognito state management to check which user has already a policy in place for IoT.
B4. Automatic up-scaling of Kinesis SHARDS due user sign-in
B5. FORGET MY PASSWORD 

(C) SCOREBOARD USER EXPERIENCE
C1. Login
C2. Game Configuration
      Type of game:
        Time-constrained (provide the duration of the game and configure a countdown) && EVERYBODY STARTS TOGETHER (websocket)
        One-Trial (custom:hasAlreadyPlayed is set to 0 (false) when )
        Multiples trials (custom:hasAlreadyPlayed is set to 0 (false) only when defined by the manager).
C3. START GAME (just for websocket)

(D) UX
D1. Build the firing station
D2. Build the missile
D3. Include response to ENTER at the 


API-GATEWAY-USER
/spaceinvaders/config  -> { UserPoolId, UserPoolURL, identityPoolId,iothost, ClientId(??-> TEM QUE SER UNICO???) }
/spaceinvaders/session -> Returns session data 
/spaceinvaders/status -> posts a status to API gateway, and then to kinesis

API-GATEWAY-ADMIN
  resetUser(username);
  

RESEARCH
- An applet, signed with valid certificate, to act as a insider-browser webserver


=== statistics ====
{ startTime: 2018-07-11T16:09:18.074Z,
  endTime: 2018-07-11T16:14:56.563Z,
  itemCount: 1279,
  minIntervalBetweenItemsInMs: 18,
  maxIntervalBetweenItemsInMs: 3489,
  averageTimeBetweenItemsInMs: 264.65129007036745 }

{ startTime: 2018-07-12T13:43:08.024Z,
  endTime: 2018-07-12T13:48:55.552Z,
  itemCount: 4102,
  minIntervalBetweenItemsInMs: 19,
  maxIntervalBetweenItemsInMs: 9034,
  averageTimeBetweenItemsInMs: 84.72159921989274 }




  === LIMITS ANALYSIS ===

  1 user posts an event at each 19 ms in averageTimeBetweenItemsInMs
  1 event has the approximated size of 120 bytes

  (A) KINESIS
  1MiB/sec for ingestion
  2MiB/sec for consumption
  No upper limit for # of shards
  No upper limit # of streams
  5 read per second
  GetRecords can retrieve 10MiB

  - Kinesis can support 8500 simultaneous users only with 1 shards
 
  (B) DynamoDB - Free tier 25 WCU, 25 RCU
  1 RCU gives you a 1 c-read/sec for item up to 4KB - Enough to read 35 player-records/sec
  1 WCU gives you a 1 write/sec for item up to 1KB - Enough to write 7 player-records/sec

  - Free tier: 875 users for RCU, 225 users for WCU. Let's stick to 200 users

sizeForUsers = function(x) { 
  var session = {
    "ClosingTime": "2018-10-26T00:18:36.240Z",
    "GameType": "SINGLE_TRIAL",
    "OpeningTime": "2018-10-26T00:18:34.013Z",
    "SessionId": "2018-10-26T00:18:34",
    "Synchronized": false,
    "TotalSeats" : 150,
    "AvailableSeats" : 0
  };
  var user =     {
       "Level": 1,
       "Lives": 0,
       "Nickname": "fabian.martins",
       "Score": 185,
       "Shots": 46,
       "Timestamp": "2018-10-26T15:43:19.324Z"
     };
    return JSON.stringify(session).length+x*JSON.stringify(user).length;
 };


#### TODOS
1. Fix credential expiration within awsfacade (cognito);
2. Fix allocatingGamer
3. ShardIterator antigo