'use strict';

const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();
const SSM = new AWS.SSM();
const SQS = new AWS.SQS();

const reportInvalidRecordToDLQ = function(record) {
    var sqsParameter = {
        "QueueUrl" : "https://sqs.us-east-1.amazonaws.com/232189948602/SpaceInvaders_DLQ",
        "MessageBody" : JSON.stringify(record)
    };
    SQS.sendMessage(sqsParameter, function(err,data) {
        if (err) console.log(err);
        else console.log(data);
    });
};

const readSessionParameter = function(callback) {
    SSM.getParameter( {"Name" : "/spaceinvaders/session"} , function(err,data) {
        if (err) {
            callback(new Error("Error getting session"),err);
        }
        else {
            try {
                var sessionInfo = JSON.parse(data.Parameter.Value);
            } catch (e) {
                var errorDetails = {
                    "Error" : e,
                    "ResponseFromSSM" : data
                };
                callback(new Error('Error parsing session data.'),errorDetails);
            }
            callback(null,sessionInfo);
        }
    });
};

const readSessionData = function(sessionId,callback) {
    var getParams = {
         "TableName" : "SpaceInvadersSession",
         "Key" : { "SessionId" : sessionId },
         "ConsistentRead" : true
    };
    DynamoDB.get(getParams, function(err, data) {
        if (err) {
            var errorDetails = {
                "Error" : err,
                "ParametersToDynamoDB" : getParams,
                "ResponseFromDynamoDB" : data,
            };
            callback(new Error("Error reading sessionData",errorDetails));
        }
        else {
            callback(null,data.Item);
        }
    });
};

const writeSessionData = function(sessionData, callback) {
    var putParams = {
        "TableName" : "SpaceInvadersSession",
        "Item" : sessionData,
    };
    DynamoDB.put(putParams, function(err,data) {
        if (err) {
            var errDetails = {
                "Error" : err,
                "ParametersToDynamoDB" : putParams,
                "ResponseFromDynamoDB" : data
            };
            callback(new Error("Error saving session data."),errDetails);
        }
        else callback(null,data);
    });
};

const preProcessRecords = function(sessionInfo,kinesisRecords) {
    var result = { FailedRecords : [] , WrongSessionRecords : [], AfterSessionClosingRecords : [], ReadyToProcessRecords : [] };
    kinesisRecords.forEach( (kinesisRecord) => {
        try {
            var record = JSON.parse(new Buffer(kinesisRecord.kinesis.data, 'base64').toString('ascii'));
            if (record.SessionId != sessionInfo.SessionId) result.WrongSessionRecords.push(record);
            else if (record.Timestamp > sessionInfo.ClosingTime) result.AfterSessionClosingRecords.push(record);
            else result.ReadyToProcessRecords.push(record);
        } catch(e) {
            result.FailedRecords.push(new Buffer(kinesisRecord.kinesis.data, 'base64').toString('ascii'));
        } 
    });
    return result;
};

const deallocateUser = function (username, sessionId, callback) {
    let readSessionControlParam = {
        "TableName": "SpaceInvadersSessionControl",
        "Key": { "SessionId": sessionId }
    };
    DynamoDB.get(readSessionControlParam, function (err, getData) {
        if (err) callback(err);
        else {
            let sessionControl = getData.Item;
            let userIdx = sessionControl.PlayingGamers.findIndex((e) => { return e == username });
            if (userIdx == -1) {
                // user not found. We can discard because this in general means double record delivery
                let message = "User "+username+" is playing, but not found in the list of playing gamers. Discarded.";
                callback(null,message);
            } else {
                sessionControl.PlayingGamers.splice(userIdx);
                sessionControl.FinishedGamers.push(username);
                let newNumberOfOccupiedSeats = sessionControl.OccupiedSeats - 1;
                let params = {
                    "TableName": "SpaceInvadersSessionControl",
                    "Key": { "SessionId": sessionControl.SessionId },
                    "UpdateExpression": "SET OccupiedSeats = :n, PlayingGamers = :p, FinishedGamers = :f",
                    "ConditionExpression": "OccupiedSeats = :o",
                    'ExpressionAttributeValues': {
                        ":n": newNumberOfOccupiedSeats,
                        ":p": sessionControl.PlayingGamers,
                        ":o": sessionControl.OccupiedSeats,
                        ":f": sessionControl.FinishedGamers
                    }
                };
                DynamoDB.update(params, function (err, _) {
                    if (err) {
                        let message = "Error in allocating seats.";
                        console.log(message);
                        console.log(err);
                        callback(err);
                    }
                    else callback(null, "Success deallocating " + username);
                });
            }
        }
    });
};

const processKinesisRecords =  function(kinesisRecords,callback) {
    var sessionData = null;
    // Read session from Systems Manager
	readSessionParameter( function(rspError, sessionInfo) {
   		if (rspError) callback( new Error("Error getting session."), rspError);
		else {
		    var preprocessedRecords = preProcessRecords(sessionInfo,kinesisRecords);
		    console.log('# of READY TO PROCESS records:',preprocessedRecords.ReadyToProcessRecords.length);
            console.log('# of FAILED records:',preprocessedRecords.FailedRecords.length);
            preprocessedRecords.FailedRecords.forEach( (r) => {
               reportInvalidRecordToDLQ(r); 
            });
            console.log('# of WRONG SESSION records:',preprocessedRecords.WrongSessionRecords.length);
            preprocessedRecords.WrongSessionRecords.forEach( (r) => {
               reportInvalidRecordToDLQ(r); 
            });
            console.log('# of AFTER SESSION CLOSING (discarded) records:',preprocessedRecords.AfterSessionClosingRecords.length);
            if (preprocessedRecords.ReadyToProcessRecords.length == 0) callback(null,sessionInfo);
		    else {
    			readSessionData(sessionInfo.SessionId, function(rsdError, rsdData) {
        			if (rsdError) callback(new Error("Error reading session data.",rsdError));
        			else {
        			   if (!rsdData) {
        				   // session does not exists on DynamoDB
        				   sessionData = sessionInfo; // sessionData is the data retrieved from SystemsManager
               			   writeSessionData(sessionInfo, function(wsdErr,wsdData) {
               			       if (wsdErr) callback(new Error("Error writing the sessionInfo to DynamoDB",wsdErr));
               			   });
        			   }
           			   else {
           			       // session exists on DynamoDB
           			       sessionData = rsdData;
           			   }
        			}
        			// Here we have sessionData read from DynamoDB
        			var scoreboard = null;
        			if (sessionData.Scoreboard) scoreboard = sessionData.Scoreboard;
        			else scoreboard = [];
                    var successfullyProcessedRecords = 0;
                    var zeroedGamers = [];
                    preprocessedRecords.ReadyToProcessRecords.forEach( function(record) {
                        var gamerIdx = scoreboard.findIndex ( (e) => {return e.Nickname == record.Nickname} );
                        delete record.SessionId;
                        if (gamerIdx==-1) {
                            scoreboard.push(record);
                        }
                        else {
                            scoreboard.splice(gamerIdx,1,record);
                        }
                        if (record.Lives == 0) {
                            let idxz = zeroedGamers.findIndex( (g) => { return g.Nickname == record.Nickname });
                            if (idxz == -1) zeroedGamers.push(record);
                        }
                        successfullyProcessedRecords+=1;
                    });
                    zeroedGamers.forEach( (record) => {
                        deallocateUser(record.Nickname,sessionInfo.SessionId,function(err,data) {
                            if (err) {
                                console.log("Error processing deallocating user.");
                                console.log(err);
                                let record = {
                                    "Type" : "ErrorToRetry",
                                    "Function" :  "deallocateUser",
                                    "Params" : JSON.stringify(record)
                                };
                                reportInvalidRecordToDLQ(record);
                            }
                        });
                    });
                    console.log('# of SUCCESSFULLY PROCESSED:',successfullyProcessedRecords);
                    if (successfullyProcessedRecords==0) callback(null,sessionData);
                    else {
                        // if we had any kind of update, let's store it on DynamoDB
                        sessionData.Scoreboard = scoreboard;
                        writeSessionData(sessionData, function(wsdErr,wsdData) {
                            if (wsdErr) callback(new Error("Error saving updated session data,"),wsdErr);
                            else callback(null,sessionData);
                        });
                    } 
    			});
			}
		}
	});
};

exports.handler = (event, context, callback) => {
    console.log('START TIME:',new Date());
    console.log(JSON.stringify(event));
    if (event.Records) {
        console.log("# of records RECEIVED:",event.Records.length);
        /*
        If event.Records exists, then the request is coming from Kinesis-Lambda integration
        */
        processKinesisRecords(event.Records,function(err,data) {
            var response = null;
            if (err) {
                console.log('Error in processKinesisRecords');
                console.log('-------- Nature of error ---------');
                console.log(err);
                console.log('--------- Error details ----------');
                console.log(data);
                response = {
                    statusCode: 200,
                    body: JSON.stringify({
                        "Error" : err,
                        "Details" : data
                    })
                };
            } else {
                response = {
                    statusCode: 200,
                    body: JSON.stringify(data)
                };
            }
            console.log('RESPONSE:',response);
            console.log('END TIME:',new Date());
            callback(null, response);
        });       
    } 
};