/**
 * The purpose of this function is to retrieve session data if there are seats available for the user
 * If there is not, an error shaw be returned.
 */
'use strict';

const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();
const SSM = new AWS.SSM();

const readSessionFromSSM = function (callback) {
    let param = {
        "Name": "/spaceinvaders/session"
    };
    SSM.getParameter(param,
        function (err, sessionParamResponse) {
            if (err) {
                console.log("Error reading from SSM");
                console.log(err);
                callback(new Error("Internal error reading SSM"),500);
            } else {
                let sessionData = null;
                try {
                    sessionData = JSON.parse(sessionParamResponse.Parameter.Value);
                    callback(null, sessionData);
                } catch (error) {
                    console.log("ERROR parsing sessionData.");
                    console.log(sessionData);
                    callback(new Error("Error with session configuration."), 500);
                }
            }
        });
};

const readSessionControlFromDynamoDB = function (session, callback) {
    let getParams = {
        "TableName": "SpaceInvadersSessionControl",
        "Key": { "SessionId": session },
        "ConsistentRead": true
    };
    DynamoDB.get(getParams, function (err, data) {
        if (err) {
            let errorDetails = {
                "Error": err,
                "ParametersToDynamoDB": getParams,
                "ResponseFromDynamoDB": data,
            };
            console.log(errorDetails);
            callback(new Error("Error reading sessionData from database."), 500);
        }
        else {
            console.log("Success in readSessionControlFromDynamoDB");
            console.log(data);
            callback(null, data.Item);
        }
    });
};

const allocateSeatForGamer = function (gamerUsername, session, sessionControl, callback) {
    let gamerIsAlreadyPlaying = ((sessionControl.PlayingGamers.findIndex((r) => { return (r == gamerUsername) })) != -1 ? true : false);
    let gamerHasAlreadyPlayed = ((sessionControl.FinishedGamers.findIndex((r) => { return (r == gamerUsername) })) != -1 ? true : false);
    if (gamerIsAlreadyPlaying) {
        let message = "Gamer is already playing";
        console.log(message);
        //callback(new Error("Gamer is already playing"), 422);
        callback(null,"Keep playing");
    }
    else {
        if (gamerHasAlreadyPlayed && (session.GameType == "SINGLE_TRIAL" || session.GameType == "TIME_CONSTRAINED")) {
            let message = "Session is " + session.GameType + ". Gamer has already played.";
            console.log(message);
            callback(new Error(message), 422);
        } 
        else {
            // Session is (MULTIPLE_TRIAL && gamerHasAlreadyPlayed) || !gamerHasAlreadyPlayed
            console.log("Session is (MULTIPLE_TRIAL && gamerHasAlreadyPlayed) || !gamerHasAlreadyPlayed");
            if (sessionControl.OccupiedSeats == sessionControl.TotalSeats) {
                var message = "No seats avalilable.";
                console.log(message);
                callback(new Error(message), 422);
            }
            else {
                sessionControl.PlayingGamers.push(gamerUsername);
                if (gamerHasAlreadyPlayed)
                    sessionControl.FinishedGamers = sessionControl.FinishedGamers.filter( (g) => { return g!=gamerUsername });
                let newNumberOfOccupiedSeats = sessionControl.OccupiedSeats + 1;
                let params = {
                    "TableName": "SpaceInvadersSessionControl",
                    "Key": { "SessionId": session.SessionId },
                    "UpdateExpression": "SET OccupiedSeats = :n, PlayingGamers = :p, FinishedGamers = :f",
                    "ConditionExpression": "OccupiedSeats = :o",
                    'ExpressionAttributeValues': {
                        ":n": newNumberOfOccupiedSeats,
                        ":p": sessionControl.PlayingGamers,
                        ":o": sessionControl.OccupiedSeats,
                        ":f": sessionControl.FinishedGamers
                    }
                };
                DynamoDB.update(params, function (err, data) {
                    if (err) {
                        let message = "Error in allocating seats.";        
                        console.log(message);
                        console.log(err);
                        callback(new Error(message), 422);
                    }
                    else callback(null, ("Success allocating "+gamerUsername));
                });
            }
        }
    }
};

const allocateGamer = function (gamerUsername, callback) {
    readSessionFromSSM(function (err, session) {
        if (err) callback(err,session);
        else {
            if (!session) callback(new Error('No session available.'), 422);
            else {
                console.log("SESSION FROM SSM");
                console.log(session);
                if (session.ClosingTime) {
                    let message = "Session is closed.";
                    console.log(message);
                    callback(new Error(message), 422);
                }
                else {
                    readSessionControlFromDynamoDB(session.SessionId, function (err, sessionControl) {
                        if (err) {
                            console.log(err);
                            callback(err, sessionControl);
                        } else allocateSeatForGamer(gamerUsername, session, sessionControl, function (err, data) {
                            if (err) {
                                console.log(err);
                                callback(err, data);
                            }
                            else {
                                console.log(data);
                                callback(null, data);
                            }
                        });
                    });
                }
            }
        }
    });
};


exports.handler = (event, context, callback) => {
    console.log('START TIME:', new Date());
    console.log(event);
    let response = null;
    let input = null;
    try {
        input = JSON.parse(event.body);
        console.log('Converted body');
        console.log(input);
    }
    catch(conversionError) {
        console.log("Input conversion error.");
        response = {
            statusCode: 400,
            isBase64Encoded : false,
            headers : {
                "X-Amzn-ErrorType":"InvalidParameterException"
            },
            body: JSON.stringify({
                "errorMessage": "Invalid request body",
            })
        };
        callback(null,response);
    }
    if (!input.Username || typeof input.Username!="string" || input.Username.trim()=="") {
        console.log("Invalid event");
        response = {
            statusCode: 400,
            isBase64Encoded : false,
            headers : {
                "X-Amzn-ErrorType":"InvalidParameterException"
            },
            body: JSON.stringify({
                "errorMessage": "Invalid request. Username not provided.",
            })
        };
        callback(null,response);
    } else {
        // input.Username = { Username }
        allocateGamer(input.Username, function (err, data) {
            if (err) {
                console.log(">>>",err);
                response = {
                    statusCode: data,
                    headers : {
                        "X-Amzn-ErrorType":"Error"
                    },
                    body: JSON.stringify({
                        "errorMessage": err.message
                    })
                };
                console.log("FAILURE");
                console.log(response);
                callback(null,response);
            }
            else {
                response = {
                    statusCode: 200,
                    body: {
                        "successMessage" : JSON.stringify(data)
                    }
                };
                console.log("SUCCESS");
                console.log(response);
                callback(null, response);
            }
        });
    }
};