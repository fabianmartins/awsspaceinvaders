const DEBUG = true;
const AWS_CONFIG = {
    "region" : "us-east-1",
    "API_ENDPOINT" : "https://d43ad4bb10.execute-api.us-east-1.amazonaws.com/prod/v1/"
}

/*
var AWS_CONFIG = {
    region : 'us-east-1',
    poolData : {
        UserPoolId: 'us-east-1_xxYuWHGnp',
        ClientId : '57k8p4ndqb8jgvr2722kp92qap',
        UserPoolURL : 'cognito-idp.us-east-1.amazonaws.com/us-east-1_xxYuWHGnp'
    },
    identityPoolId : 'us-east-1:523b36f4-4c60-4e5b-82c4-d8e5524f2bcf',
    iothost : 'a1r3oju6b9qmol.iot.us-east-1.amazonaws.com'
}
*/