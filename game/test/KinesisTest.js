'use strict';
const AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';
const KINESIS_CONFIG = {
    StreamName : 'SpaceInvaders_InputStream',
    ShardId : 'shardId-000000000000'
};

var kinesis = new AWS.Kinesis();
var currentShardIterator = null;
var interval = null;
var count = 0;
kinesis.getShardIterator( {
    StreamName: KINESIS_CONFIG.StreamName,
    ShardId : KINESIS_CONFIG.ShardId,
    ShardIteratorType : 'LATEST'
}, function(err,data) {
    if (err) console.log('ERROR getShardIterator:',err);
    else {
        currentShardIterator = data.ShardIterator;
        interval = setInterval( exploreKinesis, 500);
    }
});


function GameStatisticsManager() {
    this.statisticsElements = {
        startTime : null,
        endTime : null,
        itemCount : 0,
        minIntervalBetweenItemsInMs : 999999999,
        maxIntervalBetweenItemsInMs : -1,
    }
    this.lastItem = null;
    this.started = false;
};
GameStatisticsManager.prototype.addItem = function(item)  {
    if (!this.started) {
        //  this is the first item
        this.statisticsElements.startTime = new Date(item.Timestamp);
        this.started = true;
    } else
    // we already have processed one item 
    {  
        if (item.Timestamp > this.lastItem.Timestamp) // Avoid processing repeated items
        {
            var intervalCandidate = new Date(item.Timestamp) - new Date(this.lastItem.Timestamp);
            if (this.statisticsElements.minIntervalBetweenItemsInMs > intervalCandidate) this.statisticsElements.minIntervalBetweenItemsInMs = intervalCandidate;
            if (this.statisticsElements.maxIntervalBetweenItemsInMs < intervalCandidate) this.statisticsElements.maxIntervalBetweenItemsInMs = intervalCandidate;
            this.statisticsElements.itemCount += 1;
            console.log('ITEM:',item,(item.Lives == 0));
            if (item.Lives === 0) {
                this.statisticsElements.endTime = new Date(item.Timestamp);
                this.showStatistics();
            }
        }
    };
    this.lastItem = item;
};
GameStatisticsManager.prototype.showStatistics = function() {
    console.log('**** Statistics *****');
    this.statisticsElements['averageTimeBetweenItemsInMs'] = (this.statisticsElements.endTime - this.statisticsElements.startTime)/this.statisticsElements.itemCount;
    console.log(this.statisticsElements);
}


function Scoreboard() { 
    this.scoreboard = [];
};
Scoreboard.prototype.addItem = function(item) {
    var indexItem = this.scoreboard.findIndex( function(element) {
        return element.Nickname == item.Nickname;
    });
    if (indexItem == -1) 
        this.scoreboard.push(item);
    else {
        var element = this.scoreboard[indexItem];
        if (item.Timestamp > element.Timestamp) {
            element.Timestamp = item.Timestamp;
            element.Lives = item.Lives;
            element.Score = item.Score;
            element.Shots = item.Shots;
        }
    }
}
Scoreboard.prototype.doOverItems = function(doFunction) {
    this.scoreboard.forEach( e => doFunction(e));
}

var scoreboard = new Scoreboard();
var statistics = new GameStatisticsManager();

const exploreKinesis = function() {
    var params = {
        ShardIterator : currentShardIterator
    };
    kinesis.getRecords(params, function(err,data) {
        if (err) console.log(err);
        else {
            if(data) {
                if (data.Records) {
                    if (data.Records.length > 0) {
                        for (var i=0;i<data.Records.length;i++) {
                            console.log(JSON.parse(new String(data.Records[i].Data)));
                           // var item = JSON.parse(new String(data.Records[i].Data));
                           // scoreboard.addItem(item);
                           // statistics.addItem(item);
                        }
                    } 
                };
                currentShardIterator = data.NextShardIterator;
            }
        }
    })
}