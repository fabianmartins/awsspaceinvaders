const AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';

const SNS_CONFIG = {
    TopicName: 'SpaceInvaders_Admin',
    TopicArn : 'arn:aws:sns:us-east-1:232189948602:SpaceInvaders_Admin'
};

const sns = new AWS.SNS();

var subscribeParams = {
    Protocol: 'http',
    TopicArn: SNS_CONFIG.TopicArn
}
sns.subscribe(subscribeParams, function(err,data) {
    if (err) { 
        console.log('ERROR:',err);
    } else {
        console.log(data);
    }
});


