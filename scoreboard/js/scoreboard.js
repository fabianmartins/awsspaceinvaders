class Scoreboard {

    constructor(document) {
        this.header = ["Nickname", "Score", "Shots", "Level", "Lives"];
        this.document = document;
        this.currentSession = null;
        this.scoreboard = [];
        this.zeroedGamers = [];
        this.loopInterval = null;
        this.updateTable();
        this.awsfacade = new AWSFacade(AWS_CONFIG);
        this.SSM = null;
        this.DynamoDB = null;
    };

    login(username, password, callback) {
        if (!username || username.trim() == '' || !password || password.trim() == '')
            callback(new Error("Username and password must be provided"), null);
        else {
            let self = this;
            this.awsfacade.login(username, password, function (err, _) {
                if (err) {
                    console.log(err.message);
                    callback(new Error("Invalid login data."), null);
                }
                else {
                    self.initializeAWSServices();
                    self.loggedin = true;
                    // auto-refresh
                    setInterval( () => {
                        self.awsfacade.refreshSession( (err,session) => {
                            if (err) {
                                console.log("ERROR REFRESHING SESSION");
                                console.log(err);
                                // move to login
                                displayPageAtInitialState();
                            } else {
                               console.log("SUCCESS REFRESHING");
                            }
                        });

                    }, 30*60*1000);
                    self.loadCurrentSession(function (err, sessionData) {
                        if (err) {
                            console.log(err);
                            callback(err, null);
                        }
                        else {
                            if (sessionData) {
                                if (DEBUG) {
                                    console.log(new Date());
                                    console.log('Scoreboard.login:sessionData:', sessionData);
                                }
                                if (sessionData.Scoreboard) {
                                    sessionData.Scoreboard.forEach((record) => {
                                        self.updateArray(self.normalizeRecord(record));
                                    });
                                };
                                self.setCurrentSession(sessionData);
                                self.run();
                            }
                            callback(null, username);
                        }
                    });
                }
            });
        }
    }

    initializeAWSServices() {
        this.SSM = this.awsfacade.getSSM();
        this.DynamoDB = this.awsfacade.getDynamoDB();
    }

    setCurrentSession(sessionData) {
        this.currentSession = sessionData;
    }

    getCurrentSession() {
        return this.currentSession;
    }

    currentSessionIsOpened() {
        return (this.currentSession && !this.currentSession.ClosingTime);
    }

    /**
     * 
     * @param {*} callback function of the type (err,sessionData)
     */
    loadCurrentSession(callback) {
        let param = {
            "Name": "/spaceinvaders/session"
        };
        let self = this;
        this.SSM.getParameter(param,
            function (err, sessionParamResponse) {
                if (err) callback(err, null);
                else {
                    let sessionData = null;
                    try {
                        sessionData = JSON.parse(sessionParamResponse.Parameter.Value);
                        if (sessionData.ClosingTime)
                            // there is no open session
                            callback(null, null);
                        else {
                            // the session is open, return its data.
                            self.readSessionData(sessionData.SessionId, function (errddb, sessionDataFromDDB) {
                                if (errddb) {
                                    console.log(errddb);
                                    alert(errddb);
                                } else {
                                    callback(null, sessionDataFromDDB);
                                }
                            });
                        }
                    } catch (error) {
                        callback(error, null);
                    }
                }
            });
    }

    /**
     * Reads current session data from DynamoDB
     * @param {*} sessionId 
     * @param {*} callback 
     */
    readSessionData(sessionId, callback) {
        var getParams = {
            "TableName": "SpaceInvadersSession",
            "Key": { "SessionId": sessionId },
            "ConsistentRead": true
        };
        this.DynamoDB.get(getParams, function (err, data) {
            if (err) {
                var errorDetails = {
                    "Error": err,
                    "ParametersToDynamoDB": getParams,
                    "ResponseFromDynamoDB": data,
                };
                callback(new Error("Error reading sessionData", errorDetails));
            }
            else {
                callback(null, data.Item);
            }
        });
    };



    /**
     * 
     * @param {*} callback 
     */
    recordSessionStart(callback) {
        this.currentSession.OpeningTime = (new Date()).toJSON();
        // We are hardcoding the available session seats to keep it under the free tier for DynamoDB
        // Further implementations should be able to scale the shars
        // TODO AUTOMATE THE SESSION LIMITS
        this.currentSession.TotalSeats = 200
        var putParameterParam = {
            "Name": "/spaceinvaders/session",
            "Type": "String",
            "Value": JSON.stringify(this.currentSession),
            "Description": "Currently opened or recently closed session",
            Overwrite: true
        }
        var self = this;
        this.SSM.putParameter(putParameterParam,
            function (err, data) {
                if (err) callback(err, null);
                else {
                    let newSessionControl = {
                        "SessionId" : self.currentSession.SessionId,
                        "FinishedGamers" : [],
                        "PlayingGamers" : [],
                        "OccupiedSeats" : 0,
                        "TotalSeats" : self.currentSession.TotalSeats
                    }
                    let sessionControlParam = {
                        "TableName" : "SpaceInvadersSessionControl",
                        "Item" : newSessionControl
                    };
                    self.DynamoDB.put(sessionControlParam, function(err,_) {
                        if (err) {
                            console.log(err);
                        };
                        callback(err, self.currentSession.SessionId);
                    });
                    
                }
            }
        );
    };

    recordSessionEnding(callback) {
        this.currentSession.ClosingTime = (new Date()).toJSON();
        let currentSessionToSSM = Object.assign({},this.currentSession);
        delete currentSessionToSSM.Scoreboard;
        var putParameterParam = {
            'Name': '/spaceinvaders/session',
            'Type': 'String',
            'Value': JSON.stringify(currentSessionToSSM),
            'Description': 'Currently opened or recently closed session',
            Overwrite: true
        }
        var self = this;
        this.SSM.putParameter(putParameterParam,
            function (err, _) {
                if (err) callback(err, data);
                else {
                    let deleteParameter = {
                        'TableName': 'SpaceInvadersSessionControl',
                        'Key': {
                            "SessionId" : self.currentSession.SessionId
                        }
                    };
                    self.DynamoDB.delete(deleteParameter,
                        function (err, data) {
                            if (err) {
                                console.log("Error deleting SpaceInvadersSessionControl");
                                console.log(err);
                            }
                        }
                    );
                    // update session on DynamoDB
                    self.readSessionData(self.currentSession.SessionId, function (err, sessionInfo) {
                        if (err) callback(err, null);
                        else {
                            // Only if the session had any kind of interaction, update it
                            if (sessionInfo != null) {
                                sessionInfo.ClosingTime = self.currentSession.ClosingTime;
                                let putParameter = {
                                    'TableName': 'SpaceInvadersSession',
                                    'Item': self.currentSession
                                };
                                self.DynamoDB.put(putParameter,
                                    function (err, data) {
                                        if (err) callback(err, null);
                                        else {
                                            callback(err, data);
                                        }
                                    }
                                );
                            } else callback(err, self.currentSession);
                        }
                    });
                }
            });
    }


    start(gameTypeDetails) {
        /*  
            { SessionId : xxxxx, Timestamp: '2018-07-12T13:43:08.024Z', Nickname: 'John', Lives: 3, Score: 150, Shoots: 5, Level: 1 },
            { SessionId : xxxxx,Timestamp: '2018-07-12T13:43:08.024Z', Nickname: 'Mary', Lives: 2, Score: 125, Shoots: 5, Level: 1 },
            { SessionId : xxxxx,Timestamp: '2018-07-12T13:43:08.024Z', Nickname: 'Louis', Lives: 2, Score: 175, Shoots: 10, Level: 1 },
            { SessionId : xxxxx, Timestamp: '2018-07-12T13:43:08.024Z', Nickname: 'Jane', Lives: 3, Score: 150, Shoots: 50, Level: 1 },
            { SessionId : xxxxx,Timestamp: '2018-07-12T13:43:08.024Z', Nickname: 'Louise', Lives: 2, Score: 150, Shoots: 50, Level: 1 } 
        */
        this.scoreboard = [];
        this.zeroedGamers = [];
        this.loopInterval = null;
        this.updateTable();
        this.currentSession = gameTypeDetails;
        this.recordSessionStart(function (err, sessionName) {
            if (err) console.log(err);
            else console.log('Session started:', sessionName);
        });
        this.run();
    };

    stop() {
        var self = this;
        clearInterval(this.loopInterval);
        this.recordSessionEnding(function (err, data) {
            if (err) {
                alert(err);
                console.log(data);
            } else {
                alert('Game finished');
            }
        });
    }

    retrieveData() {
        throw new Error("NOT IMPLEMENTED");
    }

    run(preLoopFunction, intervalInMs) {
        var self = this;
        var toRun = this.retrieveData.bind(this);
        if (preLoopFunction) {
            if (typeof preLoopFunction === "function") preLoopFunction()
            else throw new Error("preLoopFunction must be a function. Type is " + (typeof preLoopFunction));
        };
        self.loopInterval = setInterval(toRun, intervalInMs);
    }

    /**
     * Remove properties that are not relevant for the Scoreboard
     * @param {*} record 
     */
    normalizeRecord(record) {
        if (record) {
            delete record.SessionId;
        };
        return record;
    }

    updateArray(newValue) {
        var gamerIdx = this.scoreboard.findIndex((e) => { return e.Nickname == newValue.Nickname });
        if (gamerIdx == -1) {
            this.scoreboard.push(newValue);
            this.updateTable();
        }
        else {
            var gamerItem = this.scoreboard[gamerIdx];
            if (newValue.Timestamp > gamerItem.Timestamp)
            // because kinesis can deliver more than once, 
            // we want to guarantee that we will only expend time in inserting new data
            {
                this.scoreboard.splice(gamerIdx, 1, newValue);
                this.updateTable();
            }
        }
    }

    updateTable() {
        var self = this;
        this.sort(function () {
            var table = self.document.createElement("table");
            table.setAttribute('id', "scoreboardtable");
            var tr = table.insertRow(-1);

            for (var i = 0; i < self.header.length; i++) {
                var th = self.document.createElement("th");
                th.innerHTML = self.header[i];
                tr.appendChild(th);
            }

            // ADD JSON DATA TO THE TABLE AS ROWS.
            for (var i = 0; i < self.scoreboard.length; i++) {
                tr = table.insertRow(-1);
                for (var j = 0; j < self.header.length; j++) {
                    var tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = self.scoreboard[i][self.header[j]];
                }
            }

            var divContainer = self.document.getElementById("scoreboard");
            if (divContainer.childNodes[0]) divContainer.removeChild(divContainer.childNodes[0]);
            divContainer.innerHTML = "";
            divContainer.appendChild(table);
        });
    }

    sort(callback) {
        this.scoreboard.sort(GameUtils.scoreboardSortingFunction);
        callback();
    };
}