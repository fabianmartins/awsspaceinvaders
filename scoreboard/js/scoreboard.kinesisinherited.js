class ScoreboardKinesis extends Scoreboard {

    initializeAWSServices() {
        super.initializeAWSServices();
        this.kinesis = this.awsfacade.getKinesisDataStream();
    }

    getInitializeShardIteratorFunction() {
        var self = this;
        var initializeShardIteratorFunction = function () {
            self.kinesis.getShardIterator({
                StreamName: 'SpaceInvaders_InputStream',
                ShardId: 'shardId-000000000000',
                ShardIteratorType: 'LATEST'
            }, function (err, data) {
                if (err) console.log('ERROR getShardIterator:', err);
                else {
                    self.currentShardIterator = data.ShardIterator;
                }
            });
        }
        return initializeShardIteratorFunction;
    }

    run() {
        var self = this;
        super.run(self.getInitializeShardIteratorFunction(), 1000);
    }

    /*
       run() {
            var self = this;
            var preLoopFunction = function() {
                self.kinesis.getShardIterator( {
                    StreamName: 'SpaceInvaders_InputStream',
                    ShardId : 'shardId-000000000000',
                    ShardIteratorType : 'LATEST'
                }, function(err,data) {
                    if (err) console.log('ERROR getShardIterator:',err);
                    else {
                        self.currentShardIterator = data.ShardIterator;
                    }
                });
            };
            super.run(preLoopFunction,1000);
        }
    */

    retrieveData() {
        var self = this;
        var loadArray = function () {
            if (self.currentShardIterator) {
                var params = {
                    ShardIterator: self.currentShardIterator
                };
                self.kinesis.getRecords(params, function (err, data) {
                    if (err) {
                        console.log("Error reading from Kinesis");
                        console.log(err);
                        throw err;
                    }
                    else {
                        if (data) {
                            if (data.Records && data.Records.length > 0) {
                                for (var i = 0; i < data.Records.length; i++) {
                                    self.updateArray(self.normalizeRecord(JSON.parse(new String(data.Records[i].Data))));
                                }
                            };
                            self.currentShardIterator = data.NextShardIterator;
                            return;
                        }
                    }
                });
            } else {
                throw new Error('currentShardIterator is null');
            }
        };

        try {
            loadArray();
        } catch (error) {
            if (error.message.toUpperCase().includes("ITERATOR EXPIRED")) {
                console.log("RENEWING ITERATOR");
                (this.getInitializeShardIteratorFunction())();
                loadArray();
            } else {
                console.log(error);
            }
        };
    }
}
