wget https://github.com/aws/aws-sdk-js/releases/download/v2.259.1/browser.zip -O aws-sdk-browser.zip
wget https://raw.githubusercontent.com/aws/amazon-cognito-identity-js/master/dist/amazon-cognito-identity.min.js -O amazon-cognito-identity.min.js 
wget https://raw.githubusercontent.com/aws/amazon-cognito-identity-js/master/dist/aws-cognito-sdk.min.js -O aws-cognito-sdk.min.js
wget http://www-cs-students.stanford.edu/~tjw/jsbn/jsbn.js -O jsbn.js
wget http://www-cs-students.stanford.edu/~tjw/jsbn/jsbn2.js -O jsbn2.js
wget https://raw.githubusercontent.com/aws/amazon-cognito-identity-js/master/dist/aws-cognito-sdk.min.js -O aws-cognito-sdk.min.js
wget https://momentjs.com/downloads/moment.min.js -O moment.min.js
